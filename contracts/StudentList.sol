//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract StudentList {

    uint public studentsCount = 0;
    
    //model student
    struct Student {
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }

    //constructor for students
    constructor() {
        // createStudent(1001, "Tashi Paljor Wangchuk");
    }

    //Event
    event createStudentEvent(
        uint _id,
        uint indexed cid,
        string name,
        bool graduated
    );

    event markedGraduatedEvent (
        uint indexed cid
    );

    //for storing the student data
    mapping(uint => Student) public students; 

    //Create and add student to storage
    function createStudent(uint _studentCid, string memory _name) public returns (Student memory) {
        studentsCount++;
        students[studentsCount] = Student(studentsCount, _studentCid, _name, false);
        //trigger create event
        emit createStudentEvent(studentsCount, _studentCid, _name, false);
        return students[studentsCount];
    }

    //change graduation status of student
    function markedGraduated(uint _id) public returns (Student memory) {
        students[_id].graduated = true;
        //trigger create event
        emit markedGraduatedEvent(_id);
        return students[_id];
    }

    //Fetch student info from storage
    function findStudent(uint _id) public view returns (Student memory) {
        return students[_id];
    }
}