//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
contract SubjectList {

    uint public subjectsCount = 0;

    //subject model
    struct Subject {
        uint _id;
        string code;
        string name;
        bool retired;
    }
    //for storing the subject data
    mapping(uint => Subject) public subjects; 

    //Constructor for subjects
    constructor() {
        // createSubject("CSB203", "DApp");
    }

    //Events
    event createSubjectEvent(
        uint _id,
        string indexed code,
        string name,
        bool retired
    );

    //Event for Retired status
    event markRetiredEvent (
        uint indexed _id
    );

    //Event for Subject Code Update 
    event updateSubjectCodeEvent(
        uint indexed _id
    );

    //Create and add subject to storage
    function createSubject(string memory _code, string memory _name) public returns (Subject memory) {
        subjectsCount++;
        subjects[subjectsCount] = Subject(subjectsCount, _code, _name, false);
        emit createSubjectEvent(subjectsCount, _code, _name, false);
        return subjects[subjectsCount];
    }

    //Change retired status of the subject
    function markRetired(uint _id) public returns (Subject memory) {
        subjects[_id].retired = true;
        //trigger create event
        emit markRetiredEvent(_id);
        return subjects[_id];
    }

    //Fetch Subject
    function findSubject(uint _id) public view returns (Subject memory) {
        return subjects[_id];
    }

    //Update Subject
    function updateSubjectCode(uint _id, string memory ncode, string memory nName) public returns (Subject memory) {
        subjects[_id].code = ncode;
        subjects[_id].name = nName;
        //trigger create event
        emit updateSubjectCodeEvent(_id);
        return subjects[_id];
    }
}
