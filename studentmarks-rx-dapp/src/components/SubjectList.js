import React, { Component } from 'react'

class SubjectList extends Component {
    render() {
        return (
            <form className="p-5 border" onSubmit={(event) => {
              event.preventDefault()
              this.props.createSubject(this.code.value, this.subject.value)
            }}
            >
                <h2>Add Subject</h2>
                <label>Module Code</label>
                <input
                  id="newCODE"
                  type="text"
                  className="form-control"
                  ref={(input) => {this.code = input;}}
                  placeholder="Module Code"
                  required
                />
                <label>Module Name</label>
                <input
                  id="newSubject"
                  type="text"
                  className="form-control"
                  ref={(input) => {this.subject = input;}}
                  placeholder="Subject Name"
                  required
                />
                <button className="button btn-primary" type="submit">
                  Add
                </button>
            </form>
        );
    }
}

export default SubjectList;