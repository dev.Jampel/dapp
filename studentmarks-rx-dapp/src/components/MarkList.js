import React, {Component} from "react";

class MarkList extends Component {
    render() {
      return (
        <form
          className="border"
          onSubmit={(event) => {
            event.preventDefault();
            this.props.addMarks(this.studentid.value, this.subjectid.value, this.grades.value);
          }}
        >
          <h2>Add Marks</h2>
          <select id="studentid" className="form-select mb-2" ref={(input) =>{this.studentid = input;}}>
            <option defaultValue={true}>Open This select menu</option>
            {
                this.props.students.map((student, key) => {
                    return (
                        <option value={ student.cid}>{student.cid}{student.name}</option>
                    )
                })
            }
        </select>
        <select id="subjectid" className="form-select mb-2" ref={(input) =>{this.subjectid = input;}}>
            <option defaultValue={true}>Open This select menu</option>
            {
                this.props.subjects.map((subject, key) => {
                    return (
                        <option value={ subject.code}>{subject.code}{subject.name}</option>
                    )
                })
            }
        </select>
        <select id="grades" className="form-select mb-2" ref={(input) =>{this.grades = input;}}>
            <option defaultValue={true}>Open This select menu</option>
            <option value={"0"}>A</option>
            <option value={"1"}>B</option>
            <option value={"2"}>C</option>
            <option value={"3"}>D</option>
            <option value={"4"}>F</option>
           
         
        </select>
         <input className="form-control btn btn-success" type="submit" hidden="" />
        </form>
      );
    }
  }
  
export default MarkList;